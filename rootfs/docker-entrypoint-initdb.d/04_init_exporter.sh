#!/bin/bash
set -e

function init_exporter() {

	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
		-- To use IF statements, hence to be able to check if the user exists before
		-- attempting creation, we need to switch to procedural SQL (PL/pgSQL)
		-- instead of standard SQL.
		-- More: https://www.postgresql.org/docs/9.3/plpgsql-overview.html
		-- To preserve compatibility with <9.0, DO blocks are not used; instead,
		-- a function is created and dropped.
		CREATE OR REPLACE FUNCTION __tmp_create_user() returns void as \$\$
		BEGIN
		  IF NOT EXISTS (
		          SELECT                       -- SELECT list can stay empty for this
		          FROM   pg_catalog.pg_user
		          WHERE  usename = '${POSTGRES_EXPORTER_USER}') THEN
		    CREATE USER "${POSTGRES_EXPORTER_USER}";
		  END IF;
		END;
		\$\$ language plpgsql;

		SELECT __tmp_create_user();
		DROP FUNCTION __tmp_create_user();

		ALTER USER "${POSTGRES_EXPORTER_USER}" WITH PASSWORD '${POSTGRES_EXPORTER_PASSWORD}';
		ALTER USER "${POSTGRES_EXPORTER_USER}" SET SEARCH_PATH TO ${POSTGRES_EXPORTER_USER},pg_catalog;

		-- If deploying as non-superuser (for example in AWS RDS), uncomment the GRANT
		-- line below and replace <MASTER_USER> with your root user.
		-- GRANT ${POSTGRES_EXPORTER_USER} TO <MASTER_USER>;
		CREATE SCHEMA IF NOT EXISTS ${POSTGRES_EXPORTER_USER};
		GRANT USAGE ON SCHEMA ${POSTGRES_EXPORTER_USER} TO ${POSTGRES_EXPORTER_USER};
		GRANT CONNECT ON DATABASE postgres TO ${POSTGRES_EXPORTER_USER};

		CREATE OR REPLACE FUNCTION get_pg_stat_activity() RETURNS SETOF pg_stat_activity AS
		\$\$ SELECT * FROM pg_catalog.pg_stat_activity; \$\$
		LANGUAGE sql
		VOLATILE
		SECURITY DEFINER;

		CREATE OR REPLACE VIEW ${POSTGRES_EXPORTER_USER}.pg_stat_activity
		AS
		  SELECT * from get_pg_stat_activity();

		GRANT SELECT ON ${POSTGRES_EXPORTER_USER}.pg_stat_activity TO ${POSTGRES_EXPORTER_USER};

		CREATE OR REPLACE FUNCTION get_pg_stat_replication() RETURNS SETOF pg_stat_replication AS
		\$\$ SELECT * FROM pg_catalog.pg_stat_replication; \$\$
		LANGUAGE sql
		VOLATILE
		SECURITY DEFINER;

		CREATE OR REPLACE VIEW ${POSTGRES_EXPORTER_USER}.pg_stat_replication
		AS
		  SELECT * FROM get_pg_stat_replication();

		GRANT SELECT ON ${POSTGRES_EXPORTER_USER}.pg_stat_replication TO ${POSTGRES_EXPORTER_USER};
	EOSQL
}

if [ ! -z "${POSTGRES_EXPORTER_USER}" ] &&  [ ! -z "${POSTGRES_EXPORTER_PASSWORD}" ]
then
	echo "Init config for exporting metrics"
	init_exporter
fi