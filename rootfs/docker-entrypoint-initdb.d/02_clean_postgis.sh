#!/bin/bash
set -e

function clean_postgis() {

	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
		DROP EXTENSION IF EXISTS postgis_tiger_geocoder;
		DROP SCHEMA IF EXISTS tiger;
		DROP SCHEMA IF EXISTS tiger_data;

		DROP EXTENSION IF EXISTS postgis_topology;
		DROP SCHEMA IF EXISTS topology;

	EOSQL
}

echo "Cleaning extensions and schemas"
clean_postgis
