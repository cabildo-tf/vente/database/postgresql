# VENTE Postgres
Base de datos PostgreSQL para almacenar datos VENTE.

Se ha incluido en la base de datos la librería de ArcGIS st_geometry para poder
crear desde ArcGIS Pro geodatabases enterprise.

Aunque se use geometría PostGIS es necesario añadir la libería para poder crear
la base de datos.

Se usa [mdillon/postgis](https://hub.docker.com/r/mdillon/postgis/) como imagen 
base para la generación del docker.

# Backup
Se añade configuración para realizar backups desde gitlab.com
